
import 'package:flutter/material.dart';

void main() {
  runApp(RegLoginPage());
}

class RegLoginPage extends StatefulWidget {
  @override
  _RegLoginPageState createState() => _RegLoginPageState();
}

class _RegLoginPageState extends State<RegLoginPage> {

  static const _REG = 'register';
  static const _LOGIN = 'login';

  // 全局key，用于获取Form表单组件
  GlobalKey<FormState> regLoginKey = GlobalKey<FormState>();

  // 用户名与密码
  String userName;
  String password;

  void operate(String operator) {
    // 读取当前Form状态
    var formState = regLoginKey.currentState;

    // 验证Form表单
    if(formState.validate()) {
      formState.save();// 保存当前状态
      print('userName = $userName' + ', password = $password');
      if(operator == _REG) {
        register();
      } else if(operator == _LOGIN) {
        login();
      }
    }
  }

  // 验证登录
  void login() {

  }

  // 验证注册
  void register() {
    // 读取当前状态

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('登录与注册页面'),
        ),
        body: Column(
          children: [
            // 1. 商标或名称
            Container(
              height: 120.0,
              child:Center(
                child: Text(
                  '公司名称或Logo',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22.0,
                    fontStyle: FontStyle.normal,
                    color: Colors.black,
                  ),
                ),
              )
            ),
            // 2. 输入框
            Container(
              // 添加内边框
              padding: const EdgeInsets.all(16.0),
              // 添加Form表单
              child: Form(
                key: regLoginKey,
                child: Column(
                  children: [
                    // 文本输入框组件
                    TextFormField(
                      // 装饰器
                      decoration: InputDecoration(
                        // 提示文本
                        labelText: '请输入用户名',
                      ),
                      maxLines: 1,
                      // 接收输入值
                      onSaved: (value) {
                        userName = value;
                      },
                      validator: (value) {
                        return value.length == 0 ? '请输入用户名': null;
                      },
                      onFieldSubmitted: (value) {},
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: '请输入密码',
                      ),
                      obscureText: true,// 文本模糊显示
                      maxLines: 1,
                      // 验证表单方法
                      validator: (value) {
                        return value.length == 0 ? '请输入密码': value.length < 6 ? '密码长度不够': null;
                      },
                      onSaved: (value) {
                        password = value;
                      },
                    )
                  ],
                ),
              ),
            ),
            // 3. 注册与登录按钮
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // 注册按钮
                SizedBox(
                  width: 160,
                  height: 42,
                  // 添加登录按钮
                  child: FlatButton(
                    onPressed: () => operate(_REG),
                    color: Colors.grey,
                    child: Text(
                      '注册',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ),
                // 登录按钮
                SizedBox(
                  width: 160,
                  height: 42,
                  // 添加登录按钮
                  child: RaisedButton(
                    onPressed: () => operate(_LOGIN),
                    child: Text(
                      '登录',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}