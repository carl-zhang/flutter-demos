import 'package:bottom_nav/sub_pages/page_first.dart';
import 'package:bottom_nav/sub_pages/page_second.dart';
import 'package:bottom_nav/sub_pages/page_third.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bottom Navigation',
      home: MyHomePage(title: 'Home Page'),
      routes: {
        '/first': (BuildContext context) => FirstPage(),
        '/second':(BuildContext context) => SecondPage(),
        '/third':(BuildContext context) => ThirdPage(),
      },
      // initialRoute: '/first',
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final firstPage = FirstPage();
  final secondPage = SecondPage();
  final thirdPage = ThirdPage();

  int _selectedNavIndex = 0;

  _currentItemPage() {
    switch(_selectedNavIndex) {
      case 0:
        return firstPage;
      case 1:
        return secondPage;
      case 2:
        return thirdPage;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 1. title部分
      appBar: AppBar(
        title: Text(widget.title),
      ),
      // 2. 主内容部分
      body: _currentItemPage(),
      // 3. 底部导航部分
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) {
          setState(() {
            _selectedNavIndex = index;
          });
        },
        currentIndex: _selectedNavIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.black,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '首页',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: '设置',
          ),
        ],
        backgroundColor: Colors.blue,
      ),
      // 悬浮按钮部分
      floatingActionButton: Container(
        padding: EdgeInsets.all(4),
        margin: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(60),
          color: Colors.white,
        ),
        child: FloatingActionButton(
          onPressed: () {
            setState(() {
              _selectedNavIndex = 1;
            });
          },
          child: Icon(Icons.add),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
