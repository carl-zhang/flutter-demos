import 'package:appbar_nav/subpages/page_first.dart';
import 'package:appbar_nav/subpages/page_second.dart';
import 'package:appbar_nav/subpages/page_third.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter AppBar Nav',
      home: MyHomePage(title: 'AppBar Nav'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin{
  var _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(
      length: 3,
      vsync: this,
    );
  }

  @override
  void dispose() {
    super.dispose();
    this._controller.dispose();
  }

  final firstPage = FirstPage();
  final secondPage = SecondPage();
  final thirdPage = ThirdPage();


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        bottom: TabBar(
          isScrollable: true,
          controller: _controller,
          tabs: [
            Tab(
              icon: Icon(Icons.account_circle),
              text: 'First',
            ),
            Tab(
              icon: Icon(Icons.home),
              text: 'Second',
            ),
            Tab(
              icon: Icon(Icons.message),
              text: 'Third',
            )
          ],
        ),
      ),
      body: TabBarView(//与TabBar一一对应
        controller: _controller,// 关联使用相同controller的TabBar
        children: [
          firstPage,
          secondPage,
          thirdPage,
        ],
      ),
    );
  }
}
