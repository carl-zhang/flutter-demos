# flutter-demo

#### 介绍
创建与保存常用界面模型
1. 登录与注册页面(register_login)
[register-login](./register_login/README.md)

2. 底部导航效果图(bottom_nav)
[bottom-navigation](./bottom_nav/README.md)

3. 顶部导航效果图(appbar_nav)
[appbar-nav](./appbar_nav/README.md)